var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var WebpackNotifierPlugin = require('webpack-notifier');
var FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
var autoprefixer = require('autoprefixer');

if (typeof process.env.NODE_ENV === 'undefined') {
    process.env.NODE_ENV = (process.argv.indexOf('-p') === -1 ? 'development' : 'production');
}

var sassLoaders = [
    'css-loader?minimize&sourceMap&url=false',
    'postcss-loader',
    'sass-loader'
];

var BUILD_DIR = path.resolve(__dirname, 'themes/Gypsum/static');
var APP_DIR = path.resolve(__dirname, 'themes/Gypsum/resources');

var config = {
    devtool: 'source-map',
    entry: APP_DIR + '/js/main.js',
    output: {
        path: BUILD_DIR + '/js/',
        filename: '[name].js'
    },
    module: {
        loaders : [
            {
                test : /\.js?/,
                include : APP_DIR,
                exclude: /(node_modules|bower_components)/,
                loader : 'babel-loader'
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: sassLoaders.join('!')
                })
            },
            {
                test: /\.txt$|.vert$|.frag$/,
                include : APP_DIR,
                exclude: /(node_modules|bower_components)/,
                loader : 'raw-loader'
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('../css/screen.css'),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer({
                        browsers: ['> 1%', 'ie >= 9'],
                        grid: true
                    })
                ]
            }
        })
    ]
};
if (typeof process.env.NODE_ENV === 'production') {
    config.plugins.push(new webpack.optimize.UglifyJsPlugin({
        compressor: { warnings: false }
    }));
}

// if (typeof process.env.NODE_ENV !== 'production') {
//     config.plugins.push(new WebpackNotifierPlugin({ alwaysNotify: true }));
// }
if (process.argv.indexOf('-w') !== -1) {
    config.stats = false;
    config.plugins.push(new FriendlyErrorsWebpackPlugin());
}

module.exports = config;