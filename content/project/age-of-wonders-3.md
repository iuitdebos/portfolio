+++
date = "2017-05-10T11:19:31+02:00"
draft = true
title = "Age of Wonders 3"
teams = ["Triumph Studios"]
tags = ["Game", "Strategy", "Design"]

+++

# Age of Wonders 3
Age of Wonders 3 is a 4X strategy game with various RPG elements, set in a fantasy world originally created over fifteen years ago, made by Triumph Studios. I was lucky enough to be able to work with the team during the hectic last three months before launch, followed by another three months supporting the release and making new content. The game was a big success, and two expansions followed the initial release.

## Dragon's Throne
One of the highlights of my time there as Game Design intern, was creating the Dragon's Throne scenario from start to finish. As the exclusive map for the game's Deluxe Edition, I designed it for fans and hardcore players. This epic 8 player XL map set near a deadly volcano filled with dragons is sure to provide the player with at least 8 hours of game time per playthrough.

## Other duties
Prior to launch, I worked a lot on balancing and finalising some of the campaign maps and scenarios, created some new mechanics (the Mystical City Upgrades and city defenses), unit balancing and QA. Post-launch I worked on some new content including narrative for the campaign and some more scenarios, as well as more QA and working through feedback.